#include "ocean.hpp"

using namespace battleship;

auto main() -> int {
    Ocean ocean;
    ocean.begin_battle();
}
