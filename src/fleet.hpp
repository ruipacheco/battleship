#include <array>
#include <vector>
#include <string>

namespace battleship {

    // Define the navy the ship belongs to.
    enum struct Navy {
        Red,
        Blue
    };

    // Define boat types.
    enum struct Type {
        Carrier,
        Destroyer,
        Frigate,
        Submarine
    };

    // Define whether a ship is horizontal or vertical on the board.
    enum struct Orientation {
        Horizontal,
        Vertical
    };

    // A ship. Positioning is deduced from its type and location.
    struct Ship {
        Ship(Navy navy, Type type);
        ~Ship() = default;

        // Deploy ship in the grid.
        void sail(std::array<int, 2> bow, Orientation orientation);

        bool operator == (const Ship &ship) {
            return m_type == ship.type() && m_navy == ship.navy();
        }

        Type type() const {
            return m_type;
        }

        Navy navy() const {
            return m_navy;
        }

        // Total number of times the ship has been hit.
        std::size_t total_hits() const {
            return m_hits.size();
        }

        // Specific coordinates where the ship has been hit.
        const std::vector<std::array<int, 2>> & hits() const {
            return m_hits;
        }

        // Return true if the ship's been hit.
        bool hit(const std::array<int, 2> &coord);

        // Ship has been sunk once the number of hits matches the length of the ship.
        bool sunk() const {
            return m_hits.size() == m_length;
        }

        // Beginning of the ship.
        std::array<int, 2> bow() const {
            return m_bow;
        }

        // End of the ship.
        std::array<int, 2> stern() const {
            return m_stern;
       }

       std::string toString() const;

        private:
            std::array<int, 2> m_bow;
            std::array<int, 2> m_stern;

            // Length of ship.
            std::size_t m_length;
            // Coordinates where the ship has been hit.
            std::vector<std::array<int, 2>> m_hits;
            Type m_type;
            Orientation m_orientation;
            Navy m_navy;
    };
}
