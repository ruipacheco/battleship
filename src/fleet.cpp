#include "fleet.hpp"
#include <iostream>

using namespace battleship;

Ship::Ship(Navy navy, Type type) : m_navy{ navy }, m_type{ type } {
    switch(type) {
        case Type::Carrier:
            m_length = 6;
            break;
        case Type::Destroyer:
            m_length = 4;
            break;
        case Type::Frigate:
            m_length = 3;
            break;
        case Type::Submarine:
            m_length = 2;
            break;
    }
}

std::string Ship::toString() const {

    std::string navy;
    if (m_navy == Navy::Blue) {
        navy = "Blue";
    } else {
        navy = "Red";
    }

    std::string type;
    if (m_type == Type::Carrier) {
        type = "Carrier";
    } else if (m_type == Type::Destroyer) {
        type = "Destroyer";
    } else if (m_type == Type::Frigate) {
        type = "Frigate";
    } else if (m_type == Type::Submarine) {
        type = "Submarine";
    }

    std::string orientation;
    if (m_orientation == Orientation::Vertical) {
        orientation = "Vertical";
    } else if (m_orientation == Orientation::Horizontal) {
        orientation = "Horizontal";
    }

    std::string repr;
    repr = navy + " " + type + " from " + std::to_string(m_bow[0]) + ":" + std::to_string(m_bow[1]) + " to " + std::to_string(m_stern[0]) + ":" + std::to_string(m_stern[1]) + " laying " + orientation + " with length " + std::to_string(m_length);
    return repr;
}

void Ship::sail(std::array<int, 2> bow, Orientation orientation) {
    m_bow = bow;
    m_stern = m_bow;
    m_orientation = orientation;
    if (m_orientation == Orientation::Horizontal) {
        m_stern[0] = m_stern[0] + m_length;
    }
    if (m_orientation == Orientation::Vertical) {
        m_stern[1] = m_stern[1] + m_length;
    }
}

bool Ship::hit(const std::array<int, 2> &coord) {
    if (std::find(m_hits.begin(), m_hits.end(), coord) != m_hits.end()) {
        return false;
    }
    m_hits.push_back(coord);

    return true;
}
