#include <vector>
#include <array>
#include "fleet.hpp"

namespace battleship {

    // Where ships live.
    struct Ocean {
        // Sets up the fleets in the ocean.
        Ocean();
        ~Ocean() = default;

        // Play the actual game.
        // Shots are fired in a loop for each navy until there are no more ships. There is no friendly fire.
        void begin_battle();

        private:
            // Deployed ships.
            std::vector<Ship> m_ships;

            // Deploy the ships for a particular navy. Ships are laid left to right and top to bottom.
            void deploy(Navy navy);

            // True if a a ship can be placed in a given location without overflowing the board 
            // or taking up another ship's place.
            bool mooringIsValid(const Ship& ship) const;

            // Make sure two ships don't overlap each other.
            bool coordinatesOverlap(const std::array<int, 2>& a1, const std::array<int, 2>& a2, const std::array<int, 2>& b1, const std::array<int, 2>& b2) const;

            // Return a random location within the grid.
            std::array<int, 2> random_location() const;

            bool onSegment(std::array<int, 2> p1, std::array<int, 2> p2, std::array<int, 2> p) const;

            int orientation(std::array<int, 2> p1, std::array<int, 2> p2, std::array<int, 2> p3) const;

            bool intersect(std::array<int, 2> a1, std::array<int, 2> a2, std::array<int, 2> b1, std::array<int, 2> b2) const;
    };
}
