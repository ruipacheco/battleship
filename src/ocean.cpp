#include "ocean.hpp"
#include <algorithm>
#include <iostream>

using namespace battleship;

Ocean::Ocean() {
    std::cout << "Reticulating splines..." << std::endl;
    std::cout << std::endl;
    const auto blue = Navy::Blue;
    deploy(blue);

    const auto red = Navy::Red;
    deploy(red);
    std::cout << std::endl;
    std::cout << "The only winning move is not to play." << std::endl;
    std::cout << std::endl;
}

void Ocean::begin_battle() {
    std::size_t total_sunk = 0;

    // Blue navy starts the game.
    Navy navy = Navy::Blue;

    // Keep firing until all ships have sunk.
    while(m_ships.size() != total_sunk) {

        

        // A shot is fired.
        const auto coord = random_location();

        // Find all floating ships for the opposing navy and check if the shot hit any of them.
        for (auto & ship : m_ships) {
            if (ship.navy() == navy || ship.sunk()) {
                continue;
            }
            if (coordinatesOverlap(ship.bow(), ship.stern(), coord, coord) && ship.hit(coord)) {
                if (navy == Navy::Blue) {
                    std::cout << "Blue Navy " ;
                } else {
                    std::cout << "Red Navy ";
                }
                std::cout << "hit a ship at coordinates " << coord[0] << ":" << coord[1] << std::endl;
                if (ship.sunk()) {
                    std::cout << ship.toString() << " is sunk." << std::endl;
                    std::cout << std::endl;
                }
                break;
            }
        }
        
        // Give the other navy a turn.
        if (navy == Navy::Blue) {
            navy = Navy::Red;
        } else {
            navy = Navy::Blue;
        }
        // Count the total number of unsunk ships
        total_sunk = std::count_if(m_ships.begin(), m_ships.end(), [&](const Ship& ship){
            return ship.sunk();
        });
    }
}

void Ocean::deploy(Navy navy) {
    for(;;) {
        // Check if we reached the total number of ships for this navy, bail out if we did.
        const auto total_ships = std::count_if(m_ships.begin(), m_ships.end(), [&](const Ship& ship) {
            return ship.navy() == navy;
        });
        if (total_ships == 9) {
            break;
        }

        // Check how many ships of a type we have. Keep spinning until we have them all.
        const auto type = static_cast<Type>(rand() % 4);
        Ship ship{ navy, type };
        const auto count = std::count(m_ships.begin(), m_ships.end(), ship);

        if (type == Type::Carrier && count == 1) {
            continue;
        }

        if (type == Type::Destroyer && count == 2) {
            continue;
        }

        if (type == Type::Frigate && count == 3) {
            continue;
        }

        if (type == Type::Submarine && count == 3) {
            continue;
        }

        // Keep trying to place the ship on the grid.
        for (;;) {
            const auto location = random_location();
            const auto orientation = static_cast<Orientation>(rand() % 2);
            ship.sail(location, orientation);
            if (mooringIsValid(ship)) {
                std::cerr << ship.toString() << std::endl;
                m_ships.push_back(std::move(ship));
                break;
            }
        }
    }
}

bool Ocean::mooringIsValid(const Ship& ship) const {
    if (ship.stern()[0] > 9 || ship.stern()[1] > 9) {
        return false;
    }
    for (const auto &previousShip : m_ships) {
        const auto a1 = ship.bow();
        const auto a2 = ship.stern();
        const auto b1 = previousShip.bow();
        const auto b2 = previousShip.stern();

        if (coordinatesOverlap(a1, a2, b1, b2)) {
            return false;
        }
    }
    return true;
}

bool Ocean::coordinatesOverlap(const std::array<int, 2>& a1, const std::array<int, 2>& a2, const std::array<int, 2>& b1, const std::array<int, 2>& b2) const {
    const auto o1 = orientation(a1, a2, b1);
    const auto o2 = orientation(a1, a2, b2);
    const auto o3 = orientation(b1, b2, a1);
    const auto o4 = orientation(b1, b2, a2);
 
    // general case
    if (o1 != o2 && o3 != o4)
        return true;
 
    // special Cases
    if (o1 == 0 && onSegment(a1, b1, a2)) 
        return true;
 
    if (o2 == 0 && onSegment(a1, b2, a2)) 
        return true;
 
    if (o3 == 0 && onSegment(b1, a1, b2)) 
        return true;
 
    if (o4 == 0 && onSegment(b1, a2, b2)) 
        return true;
 
    return false;
}

bool Ocean::onSegment(std::array<int, 2> p, std::array<int, 2> q, std::array<int, 2> r) const {
    if (q[0] <= std::max(p[0], r[0]) && q[0] >= std::min(p[0], r[0]) &&
        q[1] <= std::max(p[1], r[1]) && q[1] >= std::min(p[1], r[1]))
      return true;
 
    return false;
}

int Ocean::orientation(std::array<int, 2> p1, std::array<int, 2> p2, std::array<int, 2> p3) const {
    const auto exp = (p2[1] - p1[1]) * (p3[0] - p2[0]) - (p2[0] - p1[0]) * (p3[1] - p2[1]);
 
    if (exp == 0) {
        return 0;
    }
 
    return (exp > 0) ? 1: 2; // clock or counterclock wise
}

std::array<int, 2> Ocean::random_location() const {
    const auto x = rand() % 10;
    const auto y = rand() % 10;
    return {x, y};
}
